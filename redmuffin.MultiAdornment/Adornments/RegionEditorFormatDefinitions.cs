using System.ComponentModel.Composition;
using System.Windows.Media;
using JetBrains.Annotations;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace redmuffin.MultiAdornment.Adornments
{
    // ReSharper disable StringLiteralTypo
    [UsedImplicitly]
    internal class RegionEditorFormatDefinitions
    {
        [Export( typeof ( EditorFormatDefinition ) )]
        [Name( "MA #else" )]
        [UserVisible( true )]
        public class PreprocessorDirective_Else : EditorFormatDefinition
        {
            public PreprocessorDirective_Else() {
                DisplayName = "MA #ELSE";
                ForegroundColor = Colors.Transparent;
                BackgroundColor = Colors.Honeydew;
            }
        }

        [Export( typeof ( EditorFormatDefinition ) )]
        [Name( "MA #endif" )]
        [UserVisible( true )]
        public class PreprocessorDirective_EndIf : EditorFormatDefinition
        {
            public PreprocessorDirective_EndIf() {
                DisplayName = "MA #ENDIF";
                ForegroundColor = Colors.Transparent;
                BackgroundColor = Colors.Honeydew;
            }
        }

        [Export( typeof ( EditorFormatDefinition ) )]
        [Name( "MA #endif Border" )]
        [UserVisible( true )]
        public class PreprocessorDirective_EndIfBorder : EditorFormatDefinition
        {
            public PreprocessorDirective_EndIfBorder() {
                DisplayName = "MA #ENDIF Border";
                ForegroundColor = Colors.Transparent;
                BackgroundColor = Colors.MediumTurquoise;
            }
        }

        [Export( typeof ( EditorFormatDefinition ) )]
        [Name( "MA #endregion" )]
        [UserVisible( true )]
        public class PreprocessorDirective_EndRegion : EditorFormatDefinition
        {
            public PreprocessorDirective_EndRegion() {
                DisplayName = "MA #EndRegion";
                ForegroundColor = Colors.Transparent;
                BackgroundColor = Colors.AliceBlue;
            }
        }

        [Export( typeof ( EditorFormatDefinition ) )]
        [Name( "MA #endregion Border" )]
        [UserVisible( true )]
        public class PreprocessorDirective_EndRegionBorder : EditorFormatDefinition
        {
            public PreprocessorDirective_EndRegionBorder() {
                DisplayName = "MA #EndRegion Border";
                ForegroundColor = Colors.Transparent;
                BackgroundColor = Colors.CornflowerBlue;
            }
        }

        [Export( typeof ( EditorFormatDefinition ) )]
        [Name( "MA #if" )]
        [UserVisible( true )]
        public class PreprocessorDirective_If : EditorFormatDefinition
        {
            public PreprocessorDirective_If() {
                DisplayName = "MA #IF";
                ForegroundColor = Colors.Transparent;
                BackgroundColor = Colors.Honeydew;
            }
        }

        [Export( typeof ( EditorFormatDefinition ) )]
        [Name( "MA #if Border" )]
        [UserVisible( true )]
        public class PreprocessorDirective_IfBorder : EditorFormatDefinition
        {
            public PreprocessorDirective_IfBorder() {
                DisplayName = "MA #IF Border";
                ForegroundColor = Colors.Transparent;
                BackgroundColor = Colors.MediumTurquoise;
            }
        }

        [Export( typeof ( EditorFormatDefinition ) )]
        [Name( "MA #region" )]
        [UserVisible( true )]
        public class Region : EditorFormatDefinition
        {
            public Region() {
                DisplayName = "MA #Region";
                ForegroundColor = Colors.Black;
                BackgroundColor = Colors.AliceBlue;
            }
        }

        [Export( typeof ( EditorFormatDefinition ) )]
        [Name( "MA #region Border" )]
        [UserVisible( true )]
        public class RegionBorder : EditorFormatDefinition
        {
            public RegionBorder() {
                DisplayName = "MA #Region Border";
                ForegroundColor = Colors.Transparent;
                BackgroundColor = Colors.CornflowerBlue;
            }
        }
    }
}