﻿using System.Diagnostics;
using System.IO;
using System.Xml;
using NLog;
using NLog.Config;

namespace redmuffin.MultiAdornment
{
    public static class Logging
    {
        // Note: This is needed because NLog configuration  file seems not to work in an VSIX!
        public static LoggingConfiguration CreateConfiguration() {
#if DEBUG
            const string globalThreshold = "Trace";
#else
            const string globalThreshold = "Off";
#endif

            string xml = string.Format( @"
<nlog xmlns='http://www.nlog-project.org/schemas/NLog.xsd' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
    internalLogLevel='Warn' internalLogToConsole='true' internalLogToConsoleError='true' throwExceptions='true' globalThreshold='{0}'>
    <targets>
        <target xsi:type='AsyncWrapper' name='al'>
            <target name='l' xsi:type='NLogViewer' address='udp://127.0.0.1:878'/>
        </target> 

        <target name='cTrace' xsi:type='FilteringWrapper' condition='level==LogLevel.Trace'> 
            <target name='c' xsi:type='Console' layout='${{qpc}}            ${{message}}' />
        </target> 
        <target name='cDebug' xsi:type='FilteringWrapper' condition='level==LogLevel.Debug'> 
            <target name='c' xsi:type='Console' layout='${{qpc}}        ${{message}}' />
        </target> 
        <target name='cInfo' xsi:type='FilteringWrapper' condition='level==LogLevel.Info'> 
            <target name='c' xsi:type='Console' layout='${{qpc}}    !!! ${{message}}' />
        </target> 
        <target name='cWarn' xsi:type='FilteringWrapper' condition='level==LogLevel.Warn'> 
            <target name='c' xsi:type='Console' layout='${{qpc}}    *** ${{message}}' />
        </target> 
        <target name='cError' xsi:type='FilteringWrapper' condition='level==LogLevel.Error'> 
            <target name='c' xsi:type='Console' layout='${{qpc}}    ### ${{message}} (${{stacktrace:topFrames=4}}) ${{exception:format=ToString}}' />
        </target> 

        <target xsi:type='AsyncWrapper' name='adTrace'>
            <target name='dTrace' xsi:type='FilteringWrapper' condition='level==LogLevel.Trace'> 
                <target name='d' xsi:type='Debugger' layout='${{qpc}}     ${{message}}' />
            </target> 
        </target> 
        <target xsi:type='AsyncWrapper' name='adDebug'>
            <target name='dDebug' xsi:type='FilteringWrapper' condition='level==LogLevel.Debug'> 
                <target name='d' xsi:type='Debugger' layout='${{qpc}} ${{message}}' />
            </target> 
        </target> 
        <target xsi:type='AsyncWrapper' name='adInfo'>
            <target name='dInfo' xsi:type='FilteringWrapper' condition='level==LogLevel.Info'> 
                <target name='d' xsi:type='Debugger' layout='${{qpc}} !!! ${{message}}' />
            </target> 
        </target> 
        <target xsi:type='AsyncWrapper' name='adWarn'>
            <target name='dWarn' xsi:type='FilteringWrapper' condition='level==LogLevel.Warn'> 
                <target name='d' xsi:type='Debugger' layout='${{qpc}} *** ${{message}}' />
            </target> 
        </target> 
        <target xsi:type='AsyncWrapper' name='adError'>
            <target name='dError' xsi:type='FilteringWrapper' condition='level==LogLevel.Error'> 
                <target name='d' xsi:type='Debugger' layout='${{qpc}} ### ${{message}} (${{stacktrace:topFrames=4}}) ${{exception:format=ToString}}' />
            </target> 
        </target> 

        <target name='f' xsi:type='File' fileName='${{basedir}}/redmuffin_logs/redmuffin_${{shortdate}}.log' layout='${{longdate}} ${{uppercase:${{level}}}} ${{message}}' />
        <target name='c' xsi:type='Console' layout='*CON ${{qpc}} - ${{uppercase:${{level}}}} &#009; ${{message}}' />
        <target name='d' xsi:type='Debugger' layout='*DBG ${{qpc}} ${{uppercase:${{level}}}} ${{message}}' />
    </targets>

    <rules>
        <logger name='*' minlevel='Trace' writeTo='al,cTrace,cDebug,cInfo,cWarn,cError,adTrace,adDebug,adInfo,adWarn,adError' />
    </rules>
</nlog>", globalThreshold );

            var sr = new StringReader( xml );
            XmlReader xr = XmlReader.Create( sr );
            var configuration = new XmlLoggingConfiguration( xr, null );
            Trace.TraceInformation( "NLog LoggingConfiguration created. GlobalThreshold: " + LogManager.GlobalThreshold );
            return configuration;
        }
    }
}
