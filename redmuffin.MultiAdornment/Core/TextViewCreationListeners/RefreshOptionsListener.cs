﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics.Contracts;
using System.Linq;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using NLog;
using redmuffin.MultiAdornment.OptionsPage;

namespace redmuffin.MultiAdornment.Core.TextViewCreationListeners
{
    [ContentType( "text" )]
    [Export( typeof ( IWpfTextViewCreationListener ) )]
    [TextViewRole( PredefinedTextViewRoles.Document )]
    public sealed class RefreshOptionsListener : IWpfTextViewCreationListener, IDisposable
    {
        private static DateTime _lastRefresh = DateTime.Now;

        #region Implementation of IWpfTextViewCreationListener

        /// <summary>
        ///     Called when a text view having matching roles is created over a text data model having a matching content type.
        /// </summary>
        /// <param name="textView">The newly created text view.</param>
        public void TextViewCreated( IWpfTextView textView ) {
            Contract.Assert( textView != null );
            Contract.Assert( FormatMapService != null );

            Logger.Trace( "RefreshOptionsListener" );

            _editorFormatMap = FormatMapService.GetEditorFormatMap( textView );
            _editorFormatMap.FormatMappingChanged += FormatMappingChanged;
        }

        #endregion

        /// <summary>
        ///     The format map service
        /// </summary>
        [Import] // ReSharper disable once RedundantDefaultFieldInitializer
        internal IEditorFormatMapService FormatMapService = null;

        private IEditorFormatMap _editorFormatMap;

        private void FormatMappingChanged( object sender, FormatItemsEventArgs e ) {
            Logger.Trace( "FormatMappingChanged: " + e.ChangedItems.JoinValues() );
            RefreshCachedOptions( e );
        }

        private void RefreshCachedOptions( FormatItemsEventArgs e ) {
            Contract.Requires( e != null );

            bool nothingChanged = e.ChangedItems.Count == 0;
            bool noMultiAdornmentMappingChanged = e.ChangedItems.Count > 0 && !e.ChangedItems.Any( ci => ci.StartsWith( "MA " ) );

            if ( nothingChanged || noMultiAdornmentMappingChanged )
                return;

            DateTime newTime = _lastRefresh.AddSeconds( 5 );
            DateTime currentTime = DateTime.Now;

            Logger.Trace( "Checking " + currentTime.TimeOfDay + " >= " + newTime.TimeOfDay );

            // only update once every n seconds
            if ( !( currentTime >= newTime ) )
                return;

            Options.RefreshCachedValues();

            _lastRefresh = DateTime.Now;
        }

        #region IDisposable Members

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose() {
            if ( _editorFormatMap != null ) _editorFormatMap.FormatMappingChanged -= FormatMappingChanged;
        }

        #endregion

        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
