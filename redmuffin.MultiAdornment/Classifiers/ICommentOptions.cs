﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace redmuffin.MultiAdornment.Classifiers
{
    public partial interface ICommentOptions
    {
        HashSet<string> UserComment1Symbols { get; set; }


        HashSet<string> UserComment2Symbols { get; set; }


        HashSet<string> UserComment3Symbols { get; set; }

        bool CommentHighlightingEnabled { get; set; }


        string AllUserKeywordsRegExFragment { get; set; }

        bool ContentTypeAllowed( string currentContentTypeName );
    }

    #region ICommentOptions contract binding

    [ContractClass( typeof ( ICommentOptionsContract ) )]
    public partial interface ICommentOptions {}

    [ContractClassFor( typeof ( ICommentOptions ) )]
    // ReSharper disable once InconsistentNaming
    internal abstract class ICommentOptionsContract : ICommentOptions
    {
        public HashSet<string> UserComment1Symbols { get; set; }
        public HashSet<string> UserComment2Symbols { get; set; }
        public HashSet<string> UserComment3Symbols { get; set; }
        public bool CommentHighlightingEnabled { get; set; }
        public string AllUserKeywordsRegExFragment { get; set; }

        public bool ContentTypeAllowed( string currentContentTypeName ) {
            Contract.Requires( !String.IsNullOrEmpty( currentContentTypeName ) );
            return false;
        }
    }

    #endregion
}
