﻿using System.Diagnostics.Contracts;
using System.Runtime.InteropServices;
using EnvDTE;
using EnvDTE80;
using JetBrains.Annotations;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text.Classification;
using NLog;
using redmuffin.MultiAdornment.OptionsPage;

namespace redmuffin.MultiAdornment.Core
{
    /// <summary>
    ///     This is the class that implements the package exposed by this assembly. The minimum requirement for a class to be
    ///     considered a valid package for Visual Studio is to implement the IVsPackage interface and register itself with the
    ///     shell. This package uses the helper classes defined inside the Managed Package Framework (MPF) to do it: it derives
    ///     from the Package class that provides the implementation of the IVsPackage interface and uses the registration
    ///     attributes defined in the framework to register itself and its components with the shell.
    /// </summary>
    [ProvideOptionPage( typeof ( OptionsPageGeneral ), Options.PageName, Options.DefaultCategory, 101, 106, true )]
    [ProvideProfile( typeof ( OptionsPageGeneral ), Options.PageName, "General Options", 110, 106, true, DescriptionResourceID = 110 )]

    // This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is a package.
    [PackageRegistration( UseManagedResourcesOnly = true )]

    // This attribute is used to register the information needed to show this package in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration( "#110", "#112", Constants.VersionNumber, IconResourceID = 400 )]
    [Guid( Constants.GuidPkgString )]
    public sealed class MultiAdornmentPackage : Package
    {
        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion

        private static DTE2 _dte;
        private static IVsRegisterPriorityCommandTarget _pct;
        public bool TestOption;

        /// <summary>
        ///     Default constructor of the package. Inside this method you can place any initialization code that does not require
        ///     any Visual Studio service because at this point the package object is created but not sited yet inside Visual
        ///     Studio environment. The place to do all the other initialization is the Initialize method.
        /// </summary>
        public MultiAdornmentPackage() {
            Logger.Debug( "Package constructed" );
        }

        [UsedImplicitly]
        internal static DTE2 DTE {
            get {
                Contract.Ensures( Contract.Result<DTE2>() != null );
                return _dte ?? ( _dte = ServiceProvider.GlobalProvider.GetService( typeof ( DTE ) ) as DTE2 );
            }
        }

        [UsedImplicitly]
        internal static IVsRegisterPriorityCommandTarget PriorityCommandTarget {
            get {
                return _pct ?? ( _pct = ServiceProvider.GlobalProvider.GetService( typeof ( SVsRegisterPriorityCommandTarget ) )
                    as IVsRegisterPriorityCommandTarget );
            }
        }

        /// <summary>
        ///     Initialization of the package; this method is called right after the package is sited, so this is the place where
        ///     you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize() {
            base.Initialize();
            Logger.Debug( "Package initialized" );
        }
    }
}
