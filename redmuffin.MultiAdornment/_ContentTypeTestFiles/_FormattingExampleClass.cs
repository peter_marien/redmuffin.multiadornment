﻿/* 
 * ''' ! VB XML comment             -- disabled in this file
 * ' ! VB comment                   -- disabled in this file
 * <!-- comment text here -->       -- disabled in this file
 * <!-- ! comment text here -->     -- disabled in this file
 * <!-- x comment text here -->     -- disabled in this file
 * <!-- - comment text here -->     -- disabled in this file
 * <!-- + comment text here -->     -- disabled in this file
 * <!-- ? comment text here -->     -- disabled in this file
 * <!-- Hack comment text here -->  -- disabled in this file
 * 
 * @* comment text here *@          -- disabled in this file
 * @* ! comment text here *@        -- disabled in this file
 * @* x comment text here *@        -- disabled in this file
 * @* - comment text here *@        -- disabled in this file
 * @* + comment text here *@        -- disabled in this file
 * @* ? comment text here *@        -- disabled in this file
 * @* Hack comment text here *@     -- disabled in this file
 * 
 * :// ! this should be un-modified
 * : // !  this should work fine
 * http://thisIsNotAComment.com         // + But This is
 * https://thisIsNotAComment.com
 * 
 */

#pragma warning disable 123
//? comment at the begin of line
#pragma warning restore 123

namespace redmuffin.MultiAdornment._ContentTypeTestFiles
{
    // ReSharper disable UnusedMember.Global
    public class _FormattingExampleClass
    {
        public static int MyProperty0 { get; /* my comment here */ set; }
        public static int MyProperty1 { get; /* ! my comment here */ set; }
        public static int MyProperty2 { get; /* x my comment here */ set; }
        public static int MyProperty3 { get; /* - my comment here */ set; }
        public static int MyProperty4 { get; /*+ My Comment Here! */ set; }
        public static int MyProperty5 { get; /* ? my comment here */ set; }
        public static int MyProperty6 { get; /* Hack: my comment here */ set; }

        /// <summary>
        ///     + Registers the specified configuration.
        /// </summary>
        public static void Register() {
            // Region examples:

            #region My Region Text

#if DEBUG

#else

#endif

            /*  another type of comment */

            /* another type of comment (not supported)
             * with multiple lines
             with or without asterisks at the front */

            #endregion

            //! Important
            // - a small comment
            //? Question - colored red.
            // + Big - This is a big comment
            //x Removed - formatted as strikeout.
            //TODO: Task - xxx something.
            //     Hack:       this is clearly a hack 
            // Todo: something
            // todo: something
            // ToDo: something
            // toDo: something
            // Note
            // Attention: something
            // Warning: something
            // ReSharper comment
            //!

            // ! comment

            ////int i = 0; // ! comment

            //x strikeout
            // x strikeout

            //- small comment			
            // - small comment

            //+ big comment				
            // + big comment

            //! important comment		
            // ! important comment

            //? question				
            // ? question

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();
        }

        /// <summary>
        ///     ! Valuable Information goes here
        /// </summary>
        public void ExampleMethod() {
            #region Collapsed Inner Region

            #endregion
        }

        #region Properties

        public string Property1 { get; set; }

        public string Property2 { get; set; }

        #endregion

        // x deleted
        // - small
        // + big
        // ! important
        // ? question
        // Hack: some awful code, needs refactoring

        // a regular code comment

        /// another code comment
        public string PropertyName /* + 'multi-line comment with high commas' ! */ { get; set; } /* ! and another 'multi-line comment' format */

        //  <!-- ! HTML comment -->  <div>some text</div> <!-- ? HTML comment --> <div>some text</div> <!-- x HTML comment --> <div>some text</div>

        // @* + Razor comment -- disabled in this file *@  <a href="some link">some text</a>

#if DEBUG
        public void DebugMethod() {} // - only in DEBUG mode active
#elif TEST
        public void TestMethod() {} // - only in TEST mode active
#else
        public void ReleaseMethod() {}
#endif

// ReSharper disable once CSharpWarnings::CS1030
#warning CustomCompilerWarning

#pragma checksum "filename" "{XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}" "XXXX..."
    }
}
