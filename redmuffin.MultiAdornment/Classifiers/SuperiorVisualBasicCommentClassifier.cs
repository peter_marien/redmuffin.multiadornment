﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace redmuffin.MultiAdornment.Classifiers
{
    [ContentType( "text" )]
    [Export( typeof ( IClassifierProvider ) )]
    internal class SuperiorVisualBasicCommentClassifierProvider : IClassifierProvider
    {
        [Import] // ReSharper disable once RedundantDefaultFieldInitializer
        internal IClassificationTypeRegistryService ClassificationRegistry = null;

        public IClassifier GetClassifier( ITextBuffer textBuffer ) {
            return
                textBuffer.Properties.GetOrCreateSingletonProperty(
                    () => new SuperiorVisualBasicCommentClassifier( ClassificationRegistry, new LiveCommentOptions( Comment.VisualBasicComment ) ) );
        }
    }

    public class SuperiorVisualBasicCommentClassifier : SuperiorCommentClassifierBase
    {
        public SuperiorVisualBasicCommentClassifier( IClassificationTypeRegistryService registry, ICommentOptions options ) {
            Registry = registry;
            Options = options;
            Comment = Comment.VisualBasicComment;
        }
    }
}
