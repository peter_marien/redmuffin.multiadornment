﻿namespace redmuffin.MultiAdornment
{
    public static class Constants
    {
        public const string VersionNumber = "2.2.5.1";
        public const string GuidPkgString = "e5bac5c9-8608-412f-8d15-df79ef4dae47";
        public const string GuidOptionsPageGeneral = "502004CF-E76E-4CCC-A391-8E7583FC859A";
    };
}
