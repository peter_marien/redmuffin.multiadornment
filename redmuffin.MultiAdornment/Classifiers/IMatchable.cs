﻿using System.Diagnostics.Contracts;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;

namespace redmuffin.MultiAdornment.Classifiers
{
    public partial interface IMatchable
    {
        SnapshotSpan Span { get; }
        Regex RegEx { get; }
        IClassificationTypeRegistryService Registry { get; }
        ICommentOptions Options { get; }
        Comment Comment { get; }
    }

    #region IMatchable contract binding

    [ContractClass( typeof ( IMatchableContract ) )]
    public partial interface IMatchable {}

    [ContractClassFor( typeof ( IMatchable ) )]
    // ReSharper disable once InconsistentNaming
    internal abstract class IMatchableContract : IMatchable
    {
        public SnapshotSpan Span { get; [UsedImplicitly] private set; }
        public Regex RegEx { get; [UsedImplicitly] private set; }
        public IClassificationTypeRegistryService Registry { get; [UsedImplicitly] private set; }
        public ICommentOptions Options { get; [UsedImplicitly] private set; }
        public Comment Comment { get; [UsedImplicitly] private set; }
    }

    #endregion
}
