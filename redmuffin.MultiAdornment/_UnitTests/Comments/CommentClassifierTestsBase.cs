using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using redmuffin.MultiAdornment.Classifiers;
using redmuffin.MultiAdornment.Classifiers.CommentClassifiers;

namespace redmuffin.MultiAdornment.UnitTests.Comments
{
    public class CommentClassifierTestsBase
    {
        /// <summary>
        ///     Gets the surrounding data for the different unit tests..
        /// </summary>
        /// <value>
        ///     The surrounding data to be used in the different unit test classes.
        /// </value>
        public static IEnumerable<object[]> SurroundingData {
            get {
                return new[]
                       {
                           new object[] { String.Empty, String.Empty },
                           new object[] { "        ", @"       " },
                           new object[] { "something before", String.Empty },
                           new object[] { String.Empty, @"something after" },
                           new object[] { "something before", @"AND something after" },
                           new object[] { @"an url before http://beforedomain.com", @"AND something after" },
                           new object[] { @"something before", @"AND an url after http://afterdomain.com" }
                       };
            }
        }

        protected static List<Group> FindMatches( string keyword, string start, string end, string text ) {
            Regex regEx = CommentHighlighter.CreateRegEx( start, end, keyword );

            Logging.Logger.Trace( "\tregex:\t" + regEx );

            return CommentHighlighter.FindMatches( text, regEx ).ToList();
        }
    }
}
