Release notes:

**2013-12-20 - Update: v2.2.5.1**

- added VB.Net comment & VB.Net XML comment support: ' & '''

- added support for multiple, even different 'closing' comments in one line

- added minor padding to region adornments

- added more preprocessor directives: #elif, #warning, #error, #line, #define, #undef, #pragma, #pragma warning, #pragma warning disable, #pragma warning enable, #pragma checksum

2013-12-13 - Update: v2.1.1

- fixed 'module activation/deactivation' bug

- fixed 'xml doc' bug

2013-12-13 - Update: v2.1

- fixed 'standard-comment indentation' bug (thanks to 'pen_2')

- added 2 more userdefinable special comments

2013-12-11 - Update: v2.0

- added support for Visual Studio 2012

- comment highlighting mostly rewritten

- many settings now active after re-opening document(s)

2013-12-01 - Update: v1.8

- added single line support for 'multinline comments' (sounds funny, but is useful)

- added support for XML type comments

- added support for standard code comments (to make them look alike)

- added enabling/disabling for HTML comments

- added enabling/disabling for Razor comments

- many performance optimizations, it's quite fast now

2013-11-30 - Update: v1.7

- added user comments style 1, comma separated list of user comment symbols

- added configuration for comment fontface (default = 'Segoe UI')

- internal: repackaged projects

2013-11-29 - Update: v1.6

- added configuration for italic comment highlights (default = on)

- added configuration for underline comment highlights (default = off)

2013-11-29 - Update: v1.5

- added support for single line HTML comments

- added support for single line Razor comments

- added support for selectable contentypes

- configurable which contenttypes will display region highlights 

- configurable which contenttypes will display comment highlights

2013-11-29 - Update: v1.4

- added basic comment highlighting support

- every comment type is configurable in `Fonts and Colors`

- region highlighting can be enabled or disbaled

- comment highlighting can be enabled or disbaled

- at the moment supports only Visual Studio 2013

2013-11-28 - Update: v1.3

- added VB.Net keywords `#end if` and `#end region`

- moved color options to 'Fonts and Colors'

- every keyword has it's own colorsettings

- borderthickness now configurable under MultiAdornment General options

2013-11-28 - Update: v1.2

- added options page for colors.

2013-11-27 - Update: v1.1

- highlights are now correctly indented on both sides.
