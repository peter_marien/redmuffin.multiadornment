﻿using System;
using Jwc.AutoFixture.Xunit;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;

namespace redmuffin.MultiAdornment.UnitTests
{
    // ReSharper disable JoinDeclarationAndInitializer
    public class LoggingTests
    {
        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null )
                    return _logger;
                if ( LogManager.Configuration == null )
                    LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion

        private static void LogThrowNow() {
            Logger.ErrorException(
                "Errorexception example!",
                new InvalidOperationException( "Outer exception!", new InvalidOperationException( "Inner exception!" ) ) );
        }

        [Spec]
        public void LoggerOutputTests() {
            try {
                Logger.Trace( "Trace example!" );
                Logger.Debug( "Debug example!" );
                Logger.Info( "Info example!" );
                Logger.Warn( "Warn example!" );
                Logger.Error( "Error example!" );

                LogThrowNow();
            }
            catch ( Exception e ) {
                Console.WriteLine( e );
                Assert.Fail( "No exception should be thrown! Check NLog configuration!" );
            }

            Assert.IsTrue( true, "We reached this line of code, so everything works fine" );
        }
    }
}
