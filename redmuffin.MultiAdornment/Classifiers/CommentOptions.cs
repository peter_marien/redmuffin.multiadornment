﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using NLog;

namespace redmuffin.MultiAdornment.Classifiers
{
    /// <summary>
    ///     This is a pure data class and should have no dependencies whatsoever.
    /// </summary>
    public class CommentOptions : ICommentOptions
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CommentOptions" /> class.
        /// </summary>
        public CommentOptions(
            bool commentHighlightingEnabled,
            IEnumerable<string> allowedContentTypes,
            HashSet<string> userComment1Symbols,
            HashSet<string> userComment2Symbols,
            HashSet<string> userComment3Symbols ) {
            Contract.Requires( allowedContentTypes != null );
            Contract.Requires( userComment1Symbols != null );
            Contract.Requires( userComment2Symbols != null );
            Contract.Requires( userComment3Symbols != null );

            AllowedContentTypes = new HashSet<string>( allowedContentTypes );

            CommentHighlightingEnabled = commentHighlightingEnabled;

            UserComment1Symbols = userComment1Symbols;
            UserComment2Symbols = userComment2Symbols;
            UserComment3Symbols = userComment3Symbols;

            string u1 = ConvertUserSymbolsToRegExFragment( UserComment1Symbols );
            string u2 = ConvertUserSymbolsToRegExFragment( UserComment2Symbols );
            string u3 = ConvertUserSymbolsToRegExFragment( UserComment3Symbols );

            AllUserKeywordsRegExFragment = u1 + "|" + u2 + "|" + u3;

            Logger.Trace( "Userkeywords:\t" + AllUserKeywordsRegExFragment );
        }


        public HashSet<string> AllowedContentTypes { get; set; }

        public bool CommentHighlightingEnabled { get; set; }


        public string AllUserKeywordsRegExFragment { get; set; }


        public HashSet<string> UserComment1Symbols { get; set; }


        public HashSet<string> UserComment2Symbols { get; set; }


        public HashSet<string> UserComment3Symbols { get; set; }

        public bool ContentTypeAllowed( string currentContentTypeName ) {
            bool contentTypeAllowed = AllowedContentTypes.Contains( currentContentTypeName.ToLower() );
            return contentTypeAllowed;
        }


        private static string ConvertUserSymbolsToRegExFragment( IEnumerable<string> list ) {
            Contract.Requires( list != null );
            Contract.Requires( list.Any() );

            return list.Aggregate( "", ( c, next ) => c != "" ? c + "|" + next.Trim() : next.Trim() );
        }

        #region Logger

        private static Logger _logger;


        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
