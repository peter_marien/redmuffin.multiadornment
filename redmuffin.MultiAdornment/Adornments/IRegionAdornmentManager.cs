﻿using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Editor;

namespace redmuffin.MultiAdornment.Adornments
{
    public interface IRegionAdornmentManager
    {
        IEditorFormatMap FormatMap { get; }
        IWpfTextView View { get; }
        IAdornmentLayer Layer { get; }
        IRegionOptions RegionOptions { get; }
    }
}
