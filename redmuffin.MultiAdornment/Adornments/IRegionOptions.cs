﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Windows.Media;

namespace redmuffin.MultiAdornment.Adornments
{
    public partial interface IRegionOptions
    {
        bool Enabled { get; }
        int BorderThickness { get; }
        Dictionary<string, List<Brush>> Brushes { get; set; }
        bool ContentTypeAllowed( string currentContentType );
    }

    #region IRegionOptions contract binding

    [ContractClass( typeof ( IRegionOptionsContract ) )]
    public partial interface IRegionOptions {}

    [ContractClassFor( typeof ( IRegionOptions ) )]
    // ReSharper disable once InconsistentNaming
    internal abstract class IRegionOptionsContract : IRegionOptions
    {
        // ReSharper disable UnusedAutoPropertyAccessor.Local
        public bool Enabled { get; private set; }
        public int BorderThickness { get; private set; }
        public Dictionary<string, List<Brush>> Brushes { get; set; }

        public bool ContentTypeAllowed( string currentContentType ) {
            Contract.Requires( !String.IsNullOrEmpty( currentContentType ) );
            return false;
        }
    }

    #endregion
}
