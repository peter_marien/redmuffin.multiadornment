﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace redmuffin.MultiAdornment.Classifiers
{
    [ContentType( "text" )]
    [Export( typeof ( IClassifierProvider ) )]
    internal class SuperiorStandardCommentClassifierProvider : IClassifierProvider
    {
        [Import] // ReSharper disable once RedundantDefaultFieldInitializer
        internal IClassificationTypeRegistryService ClassificationRegistry = null;

        public IClassifier GetClassifier( ITextBuffer textBuffer ) {
            return textBuffer.Properties.GetOrCreateSingletonProperty(
                () => new SuperiorStandardCommentClassifier( ClassificationRegistry, new LiveCommentOptions( Comment.StandardComment) ) );
        }
    }

    public class SuperiorStandardCommentClassifier : SuperiorCommentClassifierBase
    {
        public SuperiorStandardCommentClassifier( IClassificationTypeRegistryService registry, ICommentOptions options ) {
            Registry = registry;
            Options = options;
            // TODO: put this into the options class
            Comment = Comment.StandardComment;
        }
    }
}
