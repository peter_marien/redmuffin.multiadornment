﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using FluentAssertions;
using Jwc.AutoFixture.Xunit;
using redmuffin.MultiAdornment.Classifiers.CommentClassifiers;
using Xunit.Extensions;

namespace redmuffin.MultiAdornment.UnitTests.Comments
{
    /// <summary>
    ///     The unit tests are very compact and not 100% easy to debug but cover a lot of ground in a simple way
    /// </summary>
    public class HtmlCommentClassifierTests : CommentClassifierTestsBase
    {
        #region Html Helpermethods

        protected static void TestComment( string comment, string keyword, string prefix, string suffix ) {
            string text = prefix + comment + suffix;

            Logging.Logger.Trace( "'{0}'\r\n\tprefix:\t'{1}'\r\n\tsuffix:\t'{2}'", text, prefix, suffix );

            text = text + Environment.NewLine;
            List<Group> groups = Html( text, keyword );

            groups.Count.ShouldBeEquivalentTo( 1, "one match should be found" );
            Logging.Logger.Trace( "\tfound:\t'{0}'", groups.Single().Value );
            groups.Single().Index.ShouldBeEquivalentTo( prefix.Length, "the start of the match should be after the 'prefix'" );
            groups.Single().Value.Length.ShouldBeEquivalentTo( comment.Length, "the length should be as long as the 'comment'" );
            groups.Single().Value.Should().NotContain( @"http://", "URL's should be excluded" );
        }

        private static void TestHtmlComments( string keyword, string prefix, string suffix ) {
            string regExKeyword = keyword.Length > 1 ? "(" + keyword.ToLower() + ")" : "[" + keyword + "]";

            TestComment( @"<!-- " + keyword + " a simple html comment -->", regExKeyword, prefix, suffix );
            TestComment( @"<!--" + keyword + "a simple html comment-->", regExKeyword, prefix, suffix );
        }

        internal static List<Group> Html( string text, string keyword ) {
            return FindMatches( keyword, HtmlCommentClassifier.Start, HtmlCommentClassifier.End, text );
        }

        #endregion

        [Spec, PropertyData( "SurroundingData" )]
        public void ImportantHtmlComments1( string prefix, string suffix ) {
            TestHtmlComments( @"!", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void BigHtmlComments1( string prefix, string suffix ) {
            TestHtmlComments( @"+", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void QuestionHtmlComments1( string prefix, string suffix ) {
            TestHtmlComments( @"?", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void DeletedHtmlComments1( string prefix, string suffix ) {
            TestHtmlComments( @"x", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void SmallHtmlComments1( string prefix, string suffix ) {
            TestHtmlComments( @"-", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void UserHtmlComments1( string prefix, string suffix ) {
            TestHtmlComments( @"hAck:", prefix, suffix );
        }
    }
}
