﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Formatting;
using NLog;
using redmuffin.MultiAdornment.Core;

namespace redmuffin.MultiAdornment.Adornments
{
    public sealed class RegionAdornmentManager : IDisposable, IRegionAdornmentManager
    {
        public const string RegionAdornmentLayerName = "redmuffin.RegionAdornmentLayer";

        public RegionAdornmentManager(
            IWpfTextView view,
            IEditorFormatMapService formatMapService,
            IRegionOptions regionOptions ) {
            Contract.Requires( view != null );
            Contract.Requires( formatMapService != null );
            Contract.Requires( regionOptions != null );

            Logger.Debug( "RegionAdornmentManager" );

            try {
                DebugTimer timer = DebugTimer.Start();

                View = view;
                Layer = view.GetAdornmentLayer( RegionAdornmentLayerName );
                RegionOptions = regionOptions;
                FormatMap = formatMapService.GetEditorFormatMap( "text" );

                // HACK: refactoring needed!!!!!!!
                regionOptions.Brushes = LiveRegionOptions.Cache.GetRegionBrushes( FormatMap );

                View.LayoutChanged += OnLayoutChanged;

                timer.Stop( "Region initialize" );
            }
            catch ( Exception e ) {
                Contract.Assert( false, e.ToString() );
                // ReSharper disable once HeuristicUnreachableCode
                Logger.ErrorException( "RegionAdornment initialization error!", e );
            }
        }

        public IRegionOptions RegionOptions { get; private set; }
        public IEditorFormatMap FormatMap { get; private set; }
        public IAdornmentLayer Layer { get; private set; }
        public IWpfTextView View { get; private set; }

        #region Keywords

        /// <summary>
        /// The start keywords
        /// </summary>
        public static readonly HashSet<string> StartKeywords =
            new HashSet<string>
            {
                "#region",
                "#if",
                "#pragma warning disable",
            };

        public static readonly HashSet<string> EndKeywords =
            new HashSet<string>
            {
                // ReSharper disable StringLiteralTypo
                "#endregion",
                "#end region",
                "#endif",
                "#end if",
                "#pragma warning restore",
                // ReSharper restore StringLiteralTypo
            };

        public static readonly HashSet<string> MiddleKeywords =
            new HashSet<string>
            {
                "#else",
                "#elif",
                "#warning",
                "#error",
                "#line",
                "#define",
                "#undef",
                "#pragma",
                "#pragma checksum",
            };

        public static readonly HashSet<string> Keywords = StartKeywords.Concat( EndKeywords ).Concat( MiddleKeywords ).ToHashSet();

        #endregion

        /// <summary>
        ///     On layout change add the adornment to any reformatted lines
        /// </summary>
        private void OnLayoutChanged( object sender, TextViewLayoutChangedEventArgs e ) {
            DebugTimer timer = DebugTimer.Start();

            try {
                var regionCreator = new RegionAdornmentCreator( this );

                foreach ( ITextViewLine line in e.NewOrReformattedLines )
                    regionCreator.CreateVisuals( line );
            }
            catch ( Exception ex ) {
                Contract.Assert( false, ex.ToString() );
                // ReSharper disable once HeuristicUnreachableCode
                Logger.ErrorException( "RegionAdornment error", ex );
            }

            timer.Stop( "All Regions" );
        }

        #region IDisposable Members

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose() {
            if ( View != null ) View.LayoutChanged -= OnLayoutChanged;
        }

        #endregion

        #region Logger

        private static Logger _logger;


        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
