﻿namespace redmuffin.MultiAdornment.Adornments
{
    public enum BrushType
    {
        Region = 0,
        RegionBorder = 1,
    }
}