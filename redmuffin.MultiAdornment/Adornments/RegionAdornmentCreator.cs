using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using JetBrains.Annotations;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Formatting;
using NLog;

namespace redmuffin.MultiAdornment.Adornments
{
    public class RegionAdornmentCreator
    {
        private const int Padding = 1;
        private readonly IRegionAdornmentManager _manager;

        private Geometry _lineGeometry;
        private SnapshotSpan _lineSpan;

        public RegionAdornmentCreator( IRegionAdornmentManager manager ) {
            _manager = manager;
        }

        [UsedImplicitly]
        public Geometry DebugIndentedGeometry { get; set; }

        [UsedImplicitly]
        public GeometryDrawing DebugTemporaryHeightElement { get; set; }

        private double TopPosition {
            get { return _lineGeometry.Bounds.Top; }
        }

        private double BottomPosition {
            get { return _lineGeometry.Bounds.Bottom; }
        }

        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion

        /// <summary>
        ///     Add the RegionAdornment behind the line
        /// </summary>
        public void CreateVisuals( ITextViewLine line ) {
            Contract.Requires( line != null );

            string lowerCaseText = line.ToLowerTrimmedWhiteSpace();

            if ( !TextContainsKeyword( lowerCaseText ) ) {
                Logger.Trace( "Text didn't contain keyword: " + lowerCaseText );
                return;
            }

            Logger.Debug( "CreateVisuals" );

            int start = line.Extent.Start;
            int end = line.Extent.End;

            _lineSpan = new SnapshotSpan( _manager.View.TextSnapshot, Span.FromBounds( start, end ) );
            _lineGeometry = GetIndentedGeometry( start, end );

            Contract.Assert( _lineGeometry != null );

            string keyword = lowerCaseText.GetKeyword();

            Logger.Trace( "Found keyword: " + keyword );

            if ( lowerCaseText.IsCollapsedRegion() )
                // ReSharper disable once StringLiteralTypo
                BorderTopAndBottom( "#region", "#endregion" );
            else if ( lowerCaseText.IsStartKeyword() )
                BorderTop( keyword );
            else if ( lowerCaseText.IsEndKeyword() )
                BorderBottom( keyword );
            else if ( lowerCaseText.IsMiddleKeyword() )
                NoBorder( keyword );
        }

        private bool TextContainsKeyword( string text ) {
            return RegionAdornmentManager.Keywords.Any( text.StartsWith );
        }

        private void BorderTopAndBottom( string topName, string bottomName ) {
            Logger.Trace( "BorderTopAndBottom" );

            DrawElement( TopPosition - Padding, topName, BrushType.Region );
            DrawElement( TopPosition - ( Padding + _manager.RegionOptions.BorderThickness ), topName, BrushType.RegionBorder );
            DrawElement( BottomPosition + 1 + Padding, bottomName, BrushType.RegionBorder );
        }

        private void BorderTop( string name ) {
            Logger.Trace( "BorderTop" );

            DrawElement( TopPosition - Padding, name, BrushType.Region );
            DrawElement( TopPosition - ( Padding + _manager.RegionOptions.BorderThickness ), name, BrushType.RegionBorder );
        }

        private void NoBorder( string name ) {
            Logger.Trace( "NoBorder" );

            DrawElement( TopPosition - Padding, name, BrushType.Region );
        }

        private void BorderBottom( string name ) {
            Logger.Trace( "BorderBottom" );

            DrawElement( TopPosition - Padding, name, BrushType.Region );
            DrawElement( BottomPosition + 1 + Padding, name, BrushType.RegionBorder );
        }

        private Geometry GetIndentedGeometry( int start, int end ) {
            Contract.Requires( start >= 0 );
            Contract.Requires( end >= 0 );
            Contract.Ensures( Contract.Result<Geometry>() != null );

            Logger.Trace( string.Format( "GetIndentedGeometry: start={0}, end={1}", start, end ) );

            var visualSpan = new SnapshotSpan();

            // ? maybe IndexOf is faster
            for ( int i = start; ( i < end ); ++i ) {
                if ( _manager.View.TextSnapshot[i] == '#' ) {
                    visualSpan = new SnapshotSpan( _manager.View.TextSnapshot, Span.FromBounds( i, end ) );
                    break;
                }
            }

            Contract.Assert( _manager.View.TextViewLines != null, "_manager.View.TextViewLines != null" );
            Contract.Assert( visualSpan != null, "visualSpan != null" );

            DebugIndentedGeometry = _manager.View.TextViewLines.GetMarkerGeometry( visualSpan );

            Logger.Trace( "GetIndentedGeometry: " + DebugIndentedGeometry );

            return DebugIndentedGeometry;
        }

        private void DrawElement( double position, string name, BrushType brushType ) {
            Contract.Requires( !String.IsNullOrEmpty( name ) );

            int height = brushType == BrushType.RegionBorder ? _manager.RegionOptions.BorderThickness : -1;

            Logger.Trace( string.Format( "DrawElementCore: name={0}, {1}, position={2}, height={3}", name, brushType, position, height ) );

            DrawElementCore( GetBrush( name, brushType ), position, height );
        }

        private void DrawElementCore( Brush brush, double position, int height ) {
            Contract.Requires( brush != null );

            var rectangle =
                new Rectangle
                {
                    Fill = brush,
                    Width = CalculateWidth( _manager.View, _lineGeometry ),
                    Height = CalculateHeight( brush, _lineGeometry, height ),
                };

            Canvas.SetLeft( rectangle, _lineGeometry.Bounds.Left - Padding );
            Canvas.SetTop( rectangle, position );

            _manager.Layer.AddAdornment( AdornmentPositioningBehavior.TextRelative, _lineSpan, null, rectangle, null );
        }

        private Brush GetBrush( string name, BrushType brushType ) {
            Contract.Requires( !String.IsNullOrEmpty( name ) );

            Logger.Trace( "Getting brush for: " + name + ", " + brushType );
            // ! pragma keywords are all designed like the #if / #endif preprocessor directive at the moment

            if ( name.Contains( "#pragma" ) ) {
                if ( name.IsStartKeyword() ) return _manager.RegionOptions.Brushes["#if"][(int)brushType];
                if ( name.IsEndKeyword() ) return _manager.RegionOptions.Brushes["#endif"][(int)brushType];
            }

            // ! Middle keywords are all designed like the #else preprocessor directive at the moment
            return name.IsMiddleKeyword()
                ? _manager.RegionOptions.Brushes["#else"][(int)brushType]
                : _manager.RegionOptions.Brushes[name][(int)brushType];
        }

        private double CalculateWidth( IWpfTextView view, Geometry geometry ) {
            Contract.Requires( geometry != null );
            Contract.Requires( view != null );

            double indentationLeftAndRight = geometry.Bounds.Left*2;

            return view.ViewportWidth - indentationLeftAndRight <= 0
                ? view.ViewportWidth
                : view.ViewportWidth - indentationLeftAndRight;
        }

        private double CalculateHeight( Brush brush, Geometry geometry, int height ) {
            if ( height > 0 )
                return height;

            var temporaryElement = new GeometryDrawing( brush, null, geometry );
            temporaryElement.Freeze();

            DebugTemporaryHeightElement = temporaryElement;

            return temporaryElement.Bounds.Height + 1 + ( Padding*2 );
        }
    }
}
